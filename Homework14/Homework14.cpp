#include <iostream>
#include <string>

int main()
{
    std::string game = "Total War Warhammer 3";
    
    std::cout << game << "\n";
    std::cout << game.length() << "\n";
    std::cout << game.front() << "\n";
    std::cout << game.back() << "\n";

    return 0;
}

